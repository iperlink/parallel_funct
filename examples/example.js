const { runParallel } = require('../dist/index')

const names = [{ host: 'localhost', password: 'splinermann', user: 'root',name: 'test1' ,},
  { host: 'localhost', password: 'splinermann', user: 'root', name: 'test2' },
  { host: 'localhost', password: 'splinermann', user: 'root', name: 'test3' }]

const asyncExample = function (params) {
  const connectPromise = new Promise(
    function (resolve) {
      setTimeout(function (err) {
        resolve(Object.assign({}, params, { success: 's' }))
      }, 100)
    }
  )
  return connectPromise
}

const simpleArray = ['test1', 'test4', 'test3']
const arrayWithError = ['test1', 'test2', 'test3']

const asyncExample2 = function (params) {
  const connectPromise = new Promise(
    function (resolve, reject) {
      setTimeout(function (err) {
        if (params === 'test2') {
          reject('err')
        }
        resolve({ params, success: 's' })
      }, 100)
    }
  )
  return connectPromise
}

const cbExample = (params, cb) => {
  setTimeout(function(err) {
    if (err) {
      cb(err)
    }
    cb(null, Object.assign({}, params, { success: 's' }))
  }, 100)
}

// runParallel(names, asyncExample,'name').then(result => { console.log('with index \n',result) })
//runParallel(names, asyncExample).then(result => { console.log('without index \n',result) })
runParallel(simpleArray, asyncExample2).then(result => { console.log('simple array \n',result) })

runParallel(arrayWithError, asyncExample2)
  .then()
  .catch(result => {
    console.log('r',result),
    setTimeout(function (err) {
      console.log('hello')
    }, 1000)
  })

runParallel(names, cbExample, null, 'series', true).then(result => { console.log(result) })
