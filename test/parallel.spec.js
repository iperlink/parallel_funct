const expect = require('chai').expect
const { runParallel } = require('../dist/index')

describe('run all parallel', () => {
  const names = [
    { name: 'test1', other: 'other1' },
    { name: 'test2', other: 'other2' },
    { name: 'test3', other: 'other3' },
  ]

  const names2 = [
    { name: 'test1', other: 'other1' },
    { name: 'test2', other: false },
    { name: 'test3', other: 'other3' },
  ]

  const asyncExample = function (params) {
    const connectProomise = new Promise(
      (function (resolve, reject) {
        setTimeout(function (err) {
          if (!params.other) {
            reject({ err:'err'})
          }
          resolve({ params, success: 's' })
        }, 1)
      }))
    return connectProomise
  }

  const cbExample = (params, cb) => {
    setTimeout(function(err) {
      cb(null, Object.assign({}, params, { success: 's' }))
    }, 1)
  }

  it('should give the name of each funct', function (done) {
    const result = [
      { params: { name: 'test1', other: 'other1' }, success: 's' },
      { params: { name: 'test2', other: 'other2' }, success: 's' },
      { params: { name: 'test3', other: 'other3' }, success: 's' }
    ]
    runParallel(names, asyncExample).then(function(ret){
      expect(ret).to.deep.eql(result)
      done()
    })
  })

  it('should run with index', function (done) {
    const result = {
      test1: { params: { name: 'test1', other: 'other1' }, success: 's' },
      test2: { params: { name: 'test2', other: 'other2' }, success: 's' },
      test3: { params: { name: 'test3', other: 'other3' }, success: 's' }
    }
    runParallel(names, asyncExample, 'name').then(function(ret){
      expect(ret).to.deep.eql(result)
      done()
    })
  })

  it('should run cb in series', function (done) {
    const result = [
      { name: 'test1', other: 'other1', success: 's' },
      { name: 'test2', other: 'other2', success: 's' },
      { name: 'test3', other: 'other3', success: 's' }
    ]
  
    runParallel(names, cbExample, null, 'series', true).then(function(ret){
      expect(ret).to.deep.eql(result)
      done()
    })
  })

  it('should give error message', done => {
    runParallel(names2, asyncExample).then().catch(ret => { 
      expect(ret).to.be.an('error');
      done()
    })
  })
})

