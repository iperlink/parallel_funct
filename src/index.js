const _ = require('lodash')
const bluebird = require('bluebird')
const steed = require('steed')

const runEach = (params, functions, type) => {
  const promise = new Promise(
    (resolve, reject) => {
      steed[type](functions, (e, results) => {
        if (e) {
          reject(e)
        } else {
          if (results[0].functName) {
            const returned = {}
            const L = results.length
            for (let i = 0; i < L; i += 1) {
              returned[results[i].functName] = results[i].result
            }
            resolve(returned)
          } resolve(results)
        }
      })
    })
  return promise
}

const getFunctions = (params, templateFunction, index, promisify) => {
  const Fn = (promisify) ? bluebird.promisify(templateFunction) : templateFunction
  if (index) {
    return _.map(params, param => {
      const returnedFunct = async CB => {
        try {
          const result = await Fn(param)
          const functName = param[index] || param
          CB(null, { functName, result })
        } catch (err) {
          console.log(err)
          CB(err)
        }
      }
      return returnedFunct
    })
  }
  return _.map(params, param => {
    const returnedFunct = async CB => {
      try {
        const result = await Fn(param)
        CB(null, result)
      } catch (err) {
        CB(err)
      }
    }
    return returnedFunct
  })
}

const runParallel = async (params, template, index, type = 'parallel', promisify = false) => {
  const functions = getFunctions(params, template, index, promisify)
  try {
    const result = await runEach(params, functions, type)
    return result
  } catch (err) {
    throw new Error({err:'test'})
  }
}

exports.runParallel = runParallel
